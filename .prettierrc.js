/**
 * @type {import('prettier').Options}
 */
const config = {
  ...require('@culur/prettier-config'),
  tabWidth: 2,
};

module.exports = config;
