import { mapLimit } from 'async';
import Web3 from 'web3';
import { AbiItem } from 'web3-utils';
import erc20Abi from '../abi/erc20.json';
import input from '../data/input.json';
import '../plugins/bignumber';
import { Erc20 } from '../types';
import BigNumber from 'bignumber.js';
import { resolve } from 'path';
import csv from 'csv';
import { promises, ensureFile } from 'fs-extra';

const rpc = 'https://mainnet.infura.io/v3/43162228a3414e24bd682bc018971e1e';
const web3 = new Web3(rpc);

const getErc20Contract = (address: string) =>
  new web3.eth.Contract(erc20Abi as AbiItem[], address) as unknown as Erc20;

type Token =
  | {
      contract: Erc20;
      decimals: number;
      symbol: string;
      token: string;
    }
  | {
      decimals: 18;
      symbol: 'ETH';
    };

type Pair = Token & { wallet: string };

type Result = Pick<Pair, 'decimals' | 'symbol' | 'wallet'> & {
  balance: BigNumber;
};

const limit = 10000;

(async () => {
  console.log('Limit', limit);
  console.time('Time');

  //! Get Tokens
  const tokens: Token[] = [
    {
      decimals: 18,
      symbol: 'ETH',
    },
    ...(await mapLimit<string, Token>(
      input.tokens,
      4,
      async (token: string) => {
        const contract = getErc20Contract(token);
        const decimals = await contract.methods.decimals().call();
        const symbol = await contract.methods.symbol().call();

        return {
          contract,
          decimals: Number(decimals),
          symbol,
          token,
        };
      },
    )),
  ];

  //! Get Wallets
  const wallets = input.wallets;

  //! Get Results
  const pairs = tokens.flatMap<Pair>(token =>
    wallets.map(wallet => ({ wallet, ...token })),
  );

  const results = await mapLimit<Pair, Result>(
    pairs,
    limit,
    async (pair: Pair) => {
      const balanceRaw =
        'contract' in pair
          ? await pair.contract.methods.balanceOf(pair.wallet).call()
          : await web3.eth.getBalance(pair.wallet);
      const balance = new BigNumber(balanceRaw).shiftedBy(-pair.decimals);

      return { ...pair, balance };
    },
  );

  //! Format Balance
  const balanceTable = results.reduce(
    (acc, result) => {
      acc['Total'][result.symbol] = (
        acc['Total'][result.symbol] ?? new BigNumber(0)
      ).plus(result.balance);
      acc[result.wallet][result.symbol] = result.balance;
      return acc;
    },
    wallets.reduce(
      (acc, wallet) => {
        acc[wallet] = {};
        return acc;
      },
      { Total: {} },
    ) as Record<string, Record<string, BigNumber>>,
  );

  console.timeEnd('Time');

  //! Write CSV
  const csvData = [
    ['Wallet', ...tokens.map(token => token.symbol)],
    ...Object.entries(balanceTable).map(([key, values]) => [
      key,
      ...tokens.map(token => values[token.symbol].toFormat()),
    ]),
  ];
  const csvString = csv.stringify(csvData);

  const file = resolve(__dirname, '../data/output.csv');
  await ensureFile(file);
  await promises.writeFile(file, csvString, 'utf-8');

  process.exit(0);
})();
