import { exec } from 'child_process';
import { readFile, writeFile } from 'fs';
import { join } from 'path';
import { promisify } from 'util';
import { glob, runTypeChain } from 'typechain';

const cwd = process.cwd();
const inputDir = 'src/abi/*.json';
const outputDir = 'src/types';

(async () => {
  //! Run typechain
  console.log('Run typechain');
  const allFiles = glob(cwd, [inputDir]);
  await runTypeChain({
    cwd,
    filesToProcess: allFiles,
    allFiles,
    outDir: outputDir,
    target: 'web3-v1',
  });

  //! Update types content
  console.log('Update types.ts content');
  const typesPath = join(cwd, outputDir, 'types.ts');
  const typesContentRaw = await promisify(readFile)(typesPath, 'utf8');
  const typesContent = typesContentRaw.replace(
    /(send\(tx\?: (Non)?PayableTx)\)/gm,
    '$1, callback?: (error: Error, txHash: string) => void)',
  );
  await promisify(writeFile)(typesPath, typesContent);

  //! Format code
  console.log('Format code');
  const { stderr } = await promisify(exec)(
    `pnpm prettier ${inputDir} ${outputDir} --write`,
  );

  if (stderr) console.log(stderr);

  process.exit(0);
})();
